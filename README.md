# Desafiocris2

**Desafio Mandic 2
Documentação Desafiocris2**

**Realizar a Instalação Ansible no Ubunto;**

- sudo apt update
- sudo apt install software-properties-common
- sudo apt-add-repository --yes --update ppa:ansible/ansible
- sudo apt install ansible

**Realizar o Update Maquina;**

- apt-get update

**Criar Diretório dentro do ansible para os projetos;**

- /etc/ansible/"nome do projeto"

**Criar arquivo dentro do Diretório criado para os Hosts;**

- vim Hosts

**Dentro de vim hosts colocar os IPs das maquinas que devem ser comunicadas com o ansible;**

- 192.168.0.0

**Criar arquivo .yml esse mesmo aquivi será o playbook do ansible;**

- vim Playbook_ansible.yml

**Criar um diretório chamado templates para colocar todos os templates no formato .j2;**

- mkdir templates

**Criar uma chave secreta na AWS para comunicação do ansible com  a máquina e exportar para sua máquina ansible;**

- export AWS_SECRET_ACCESS_KEY="chave"

- export AWS_ACCESS_KEY_ID="chave"

**Verificar se as chaves foram importadas;**

- printenv

**Adicionar chaves ao seu diretório ansible;**

- ssh-add "chave".pem

**Criar o playbook;**

**Abaixo segue modelo de criação do desafio2;**


`---

- hosts: all
  become: yes
  tasks:

  - name: upgrade all packages
    yum: name=* state=latest

  - name: Disable Selinux
    selinux:
      state: disabled

  - name: Instalando o Epel
    yum: name=epel-release state=present

  - name: Instalando o Nginx
    yum: name=nginx state=present

  - name: Habilitando o start do Nginx no Boot
    service: name=nginx enabled=yes

  - name: Install remi repo.
    yum:
      name: http://rpms.remirepo.net/enterprise/remi-release-7.rpm
      state: present
  - name: Import remi GPG key.
    rpm_key:
      key: http://rpms.remirepo.net/RPM-GPG-KEY-remi
      state: present
  - name: Enable php72 repo
    ini_file:
      dest: /etc/yum.repos.d/remi-php72.repo
      section: remi-php72
      option: enabled
      value: 1

  - name: Instalando PHP
    yum:
      name: ['php-cli','php-zip','unzip','php-fpm','php-ctype','php-curl','php-dom','php-gd','php-intl','php-mbstring','php-hash','php,openssl','php-simplexml','php-soap','php-spl','php-libxml','php-xsl','php-json','php-iconv','php-mcrypt','php-bcmath','php-pdo_mysql','php-pecl-zip']

  - name: Instalando PHP
    yum: name=php state=present

  - name: Configurando wwwconf
    lineinfile:
      dest: "/etc/php-fpm.d/www.conf"
      regexp: "{{ item.regexp }}"
      line: "{{ item.line }}"
      state: present
    with_items:
      - regexp: "^user.?=.+$"
        line: "user = nginx"
      - regexp: "^group.?=.+$"
        line: "group = nginx"
      - regexp: "^listen.?=.+$"
        line: "listen = /run/php-fpm/www.sock"
      - regexp: "^;listen.owner.?=.+$"
        line: "listen.owner = nginx"
      - regexp: "^;listen.group.?=.+$"
        line: "listen.group = nginx "
      - regexp: "^;listen.mode.?=.+$"
        line: "listen.mode = {{ 0600 }}"

  - name: Instalando  dependencias do  WordPress
    yum: name=httpd state=present

  - name: Adicionando repo MariaDB 10.4
    template:
      src: repo-mariadb.j2
      dest: /etc/yum.repos.d/MariaDB.repo

  - name: yum cache
    yum: update_cache=yes

  - name: Instalação MariaDB 10.4
    yum:
      name: ['MariaDB-server']

  - name: Instalando Python
    yum:  name=MySQL-python state=present

  - name: Iniciando  MariaDB (and enable it at boot)
    service: name=mariadb state=started enabled=yes

  - name: Criando direct example.com
    file:
      path: /var/www/html/example.com
      state: directory
      owner: nginx
      group: nginx
      recurse: yes

  - name: Criação database wordpress
    mysql_db:
      name: "wordpress"
      state: present
      login_user: root
      login_password: ""
      login_unix_socket: /var/lib/mysql/mysql.sock

  - name: Criação do wordpressuser e garantindo acesso
    mysql_user:
       name: "wordpressusert"
       password: "root"
       priv: "wordpress.*:ALL"
       state: present
       login_user: root
       login_password: ""
       login_unix_socket: /var/lib/mysql/mysql.sock

  - name: Baixando o Wordpress
    get_url:
      url: https://wordpress.org/latest.tar.gz
      dest: /var/www/html/example.com/
`   template:
      src: blog.conf.j2
      dest: /etc/nginx/conf.d/blog.conf

  - name: Criando directorio loja-blog
    file:
      path: /var/www/html/loja-blog
      state: directory
      owner: nginx
      group: nginx
      recurse: yes

  - name: Criando directorio desafiocris.tk
    file:
      path: /var/www/html/desafiocris.tk
      state: directory
      owner: nginx
      group: nginx

  - name: Criando directorio tomcat
    file:
      path: /var/www/html/tomcat
      state: directory
      owner: nginx
      group: nginx

  - name: Criando index.php
    template:
      src: index.j2
      dest: /var/www/html/desafiocris.tk/index.php
      owner: nginx
      group: nginx

  - name: Instalando o Wget
    yum:  name=wget state=present

  - name: Instalando JAVA
    yum:
      name: java-1.8.0-openjdk-devel
      state: present

  - name: Adicionando grupo tomcat
    group:
      name: tomcat
  - name: Adicionando usuário
    user:
      name: tomcat
      group: tomcat
      home: /opt/tomcat
      system: yes

  - name: Download tomcat
    get_url:
      url: https://downloads.apache.org/tomcat/tomcat-9/v9.0.37/bin/apache-tomcat-9.0.37.tar.gz
      dest: /opt/tomcat
  - name: Descompactar tomcat

    unarchive:
      src: /opt/tomcat/apache-tomcat-9.0.37.tar.gz
      dest: /opt/tomcat
      remote_src: yes

  - name: Permissão tomcat
    file:
      path: /opt/tomcat
      owner: tomcat
      group: tomcat
      mode: '0777'
      recurse: yes

  - name: Adicionando arquivo systemd tomcat
    template:
      src: tomcat.service.j2
      dest: "/etc/systemd/system/tomcat.service"

  - name: Reload systemd
    systemd:
      daemon_reload: yes

  - name: Restart tomcat
    service:
      name: tomcat
      state: restarted

  - name: Alterações no user xml
    replace:
      path: /opt/tomcat/apache-tomcat-9.0.37/conf/tomcat-users.xml
      regexp: '(<user username="xxxxx" password="<must-be-changed>" roles="tomcat"/>)'
      replace: '<user username="xxxxx" password="xxxxxx" roles="tomcat"/>'

  - name: Excluindo arquivo tomcat contexto
    file:
      path: /opt/tomcat/apache-tomcat-9.0.37/webapps/manager/META-INF/context.xml
      state: absent

  - name: Adicionando arquivo de contexto
    template:
      src: tomcat.context.j2
      dest: "/opt/tomcat/apache-tomcat-9.0.37/webapps/manager/META-INF/context.xml
   file:
      path: /opt/tomcat/apache-tomcat-9.0.37/webapps/host-manager/META-INF/context.xml
      state: absent
  - name: Adicionando arquivo contexto 2

    template:
      src: tomcat.context2.j2
      dest: "/opt/tomcat/apache-tomcat-9.0.37/webapps/host-manager/META-INF/context.xml"
  - name: Restart tomcat
    service:
      name: tomcat
      state: restarted

  - name: Criando site Majento
    template:
      src: magento.conf
      dest: /etc/nginx/conf.d


  - name: Criação database wordpress
    mysql_db:
      name: "xxxxx"
      state: present
      login_user: root
      login_password: "xxxxx"

  - name: Criação do wordpressuser e garantindo acesso
    mysql_user:
       name: "xxxxx"
       password: "xxxxx"
       priv: "magento.*:ALL"
       state: present
       login_user: root
       login_password: "xxxxx"

  - name: Editando arquivo php.ini
    lineinfile:
      dest: "/etc/php.ini"
      regexp: "{{ item.regexp }}"
      line: "{{ item.line }}"
      state: present
    with_items:
    - regexp: "^memory_limit.?=.+$"
      line: "memory_limit = 2G"
    - regexp: "^max_execution_time.?=.+$"
      line: "max_execution_time = 1800"
    - regexp: "^zlib.output_compression.?=.+$"
      line: "zlib.output_compression = On"
    - regexp: "^;session.save_path.?=.+$"
      line: "session.save_path = /var/lib/php/session"

  - name: baixando o Composer
    get_url:
       url:  https://getcomposer.org/installer
       dest: /tmp/

  - name: instalando composer
    shell: cat /tmp/installer | php -- --install-dir=/usr/local/bin --filename=composer
    args:
     creates: /usr/local/bin/composer

  - name: Renomeando composer.phar para composer
    shell: mv /usr/local/bin/composer.phar /usr/local/bin/composer
    args:
     creates: /usr/local/bin/composer

  - name: Inserindo as credenciais do magento
    shell: /usr/local/bin/composer config --global http-basic.repo.magento.com ff01e3e27bbc87e9a17ec78f9590cc78 ceaf1c116dd3725138061fcd900a6025A

  - name: Criar projeto magento
    shell: /usr/local/bin/composer create-project --repository=https://repo.magento.com/ magento/project-community-editionA .
    args:
     chdir: /var/www/html/loja-blog

  - name: Setando permissões Magento
    shell: find var vendor pub/static pub/media app/etc -type f -exec chmod u+w {} \; && find var vendor pub/static pub/media app/etc   -type d -exec chmod u+w {} \; && chmod u+x bin/magento
    args:
     chdir: "/var/www/html/loja-blog"

  - name: Instalando Magento
    shell: bin/magento setup:install \
     --base-url=http://loja-blog.desafiocris.tk \
     --db-host=localhost \
     --db-name=xxxxx \
     --db-user=xxxxx \
     --db-password=xxxxx \
     --backend-frontname=xxxxx \
     --admin-firstname=xxxxx \
     --admin-lastname=xxxxxx \
     --admin-email=xxxxx  \
     --admin-user=xxxxx \
     --admin-password=xxxxx \
     --language=en_US \
     --currency=USD \
     --timezone=America/Chicago \
     --use-rewrites=1
    args:
     chdir: /var/www/html/loja-blog

  - name: Atribuindo nginx ao do magento
    file:
     path: /var/www/html/loja-blog
     owner: nginx
     group: nginx
     recurse:yes                                                                                                      325,1       
  - name: Alterando para o modo de desenvolvedor do Magento
    shell: ./magento deploy:mode:set developer
    args:
     chdir: /var/www/html/magento/bin/

  - name: Reiniciando php-fpm
    service:
       name: php-fpm
       state: restarted

  - name: Reatribuindo permissoes no session do php
    file:
     path: /var/lib/php/session
     owner: nginx
     group: nginx

  - name: Install Certbot.
    yum:
      name: [''] 
      state: present

  - name: Criação de cert wildcard
      command: "certbot --nginx -d blog.desafiocris.tk --email xxxxx --agree-tos"

  - name: Reiniciando serviço nginx
      systemd:
     state: restarted
     daemon_reload: yes
     name: nginx

  - name: Reiniciando serviço php-fpm
      systemd:
     state: restarted
     daemon_reload: yes
     name: php-fpm
                                                           
**Referencias;**

CMDB: https://www.opservices.com.br/cmdb/

Ansible: https://docs.ansible.com/

Curso na MUC: https://muc.mandic.com.br/mod/scorm/player.php?a=85currentorg=j4ydU1xOlfxps_organization&scoid=198

Curso básico de ansible: https://www.udemy.com/course/ansible-essentials-simplicity-in-automation/learn/lecture/6125460#overview

Curso MASTERNING ANSIBLE: https://www.udemy.com/course/mastering-ansible/learn/lecture/3731986#overview


Digital Ocean: https://www.digitalocean.com/community/tutorialshow-to-install-and-configure-ansible-on-centos-7
